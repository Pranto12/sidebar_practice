import { Component } from '@angular/core';
import { ActionComponent } from './components/action/action.component';
import { DetailsComponent } from './components/details/details.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'practice';

}
