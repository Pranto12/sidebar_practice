import { Component, OnInit, DoCheck } from '@angular/core';
import { ActionServiceService } from 'src/app/service/action-service.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, DoCheck {

  subText: boolean = false;
  subWrapper: boolean = false;
  sub1: boolean = false;
  sub2: boolean = false;
  sub3: boolean = false;
  mobileWrapper: boolean = false;

  constructor(private myService: ActionServiceService) { 
    this.mobileWrapper = false
  }

  ngOnInit(): void {
    
  }

  ngDoCheck(): void {
    var myWindow = window.screen.width;
    if(myWindow < 992) {
      if(this.myService.myFlag == true) {
        this.mobileWrapper = true;
      }
      else {
        this.mobileWrapper = false
        this.sub1 = false;
        this.sub2 = false;
        this.sub3 = false;
      }
    }
  }

  list1() {
    this.sub1 = true;
    this.sub2 = false;
    this.sub3 = false;
    this.subWrapper = true;
    if(this.mobileWrapper == true) {
      this.mobileWrapper = true
    }
  }

  list2() {
    this.sub2 = true
    this.sub1 = false;
    this.sub3 = false;
    this.subWrapper = true;
  }

  list3() {
    this.sub3 = true
    this.sub1 = false;
    this.sub2 = false;
    this.subWrapper = true;
  }

  none() {
    // this.sub1 = false;
    // this.sub2 = false;
    // this.sub3 = false;
    this.subWrapper = false;
  }

}
