import { Component, OnInit } from '@angular/core';
import { ActionServiceService } from 'src/app/service/action-service.service';


@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.scss']
})
export class ActionComponent implements OnInit {

  constructor(private myService: ActionServiceService) { }

  ngOnInit(): void {
  }

  toggle() {
    this.myService.myFlag = !this.myService.myFlag;
    console.log(this.myService.myFlag)
  }

}
